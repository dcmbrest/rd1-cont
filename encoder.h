// пины энкодера
#define SW 39
#define DT 34
#define CLK 35
#include "encMinim.h"
encMinim enc(CLK, DT, SW, 1, 1);
// пин clk=key, пин dt=S2, пин sw=S1, направление (0/1), тип (0/1)
void setup() {
  Serial.begin(115200);
}
void loop() {
  enc.tick();
  if (enc.isTurn()) Serial.println("turn");
  if (enc.isLeft()) Serial.println("left");
  if (enc.isRight()) Serial.println("right");
  if (enc.isLeftH()) Serial.println("leftH");
  if (enc.isRightH()) Serial.println("rightH");
  if (enc.isClick()) Serial.println("click");
}

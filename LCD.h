#include <LiquidCrystal_I2C.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Wire.h>


float temperature;
float humidity;
float pressure;
float altitude;

float const ALTITUDE = 62.0;              // Altitude at my location in meters
float const SEA_LEVEL_PRESSURE = 1013.25;  // Pressure at sea level

Adafruit_BME280 bme; // I2C

// set the LCD number of columns and rows
int lcdColumns = 20;
int lcdRows = 4;

// set LCD address, number of columns and rows
// if you don't know your display address, run an I2C scanner sketch
LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows); 
//===============================================================================
//  Initialization
//===============================================================================
void setup(void) {

  lcd.init();
  lcd.clear();          // Clear display
  lcd.backlight();      // Make sure backlight is on
  lcd.print("Reading sensor");

  bool status;

  // default settings
  status = bme.begin(0x76);  // The I2C address of the sensor is 0x76
  if (!status) {             // Loop if sensor not found
    lcd.clear();
    lcd.print("Error. Check");
    lcd.setCursor(0, 1);
    lcd.print("connections");
    while (1);
  }
  // Print non-changing info on LCD once
  lcd.clear();          // Clear display
  lcd.setCursor(0, 0);  //Set cursor to character 0 on line 0
  lcd.print("Temperature: ");
  lcd.setCursor(0, 1);  //Set cursor to line 1
  lcd.print("Humidity: ");
  lcd.setCursor(0, 2); // Set cursor to line 2
  lcd.print("Pressure: ");
  lcd.setCursor(0, 3); // Set cursor to line 3
  lcd.print("Altitude:   ");
}
//===============================================================================
//  Main
//===============================================================================
void loop() {

  getPressure();   // Get sensor data and print to LCD
  getHumidity();
  getTemperature();
  getAltitude();
  
  delay(2000);     // Update readings every 2 seconds
}
//===============================================================================
//  getTemperature - Subroutine to get and print temperature
//===============================================================================
void getTemperature()
{
  temperature = bme.readTemperature();
  // temperature = temperature * 9 / 5 + 32; // Convert C to F
  String temperatureString = String(temperature, 1); // One decimal position
  lcd.setCursor(12, 0);           // Move to start of reading
  lcd.print("       ");           // Clear old reading
  lcd.setCursor(12, 0);           // Reset cursor location
  lcd.print(temperatureString);   // Write new reading
  lcd.print((char)223);           // Degree symbol
  lcd.print("C ");
}
//===============================================================================
//  getHumidity - Subroutine to get and print humidity
//===============================================================================
void getHumidity()
{
  humidity = bme.readHumidity();
  String humidityString = String(humidity, 0);
  lcd.setCursor(12, 1);
  lcd.print("       ");
  lcd.setCursor(12, 1);
  lcd.print(humidityString);
  lcd.print(" %");
}
//===============================================================================
//  getPressure - Subroutine to get and print pressure
//===============================================================================
void getPressure()
{
  pressure = bme.readPressure();
  pressure = bme.seaLevelForAltitude(ALTITUDE, pressure);
  pressure = pressure * 0.0075006156 ; //3386.39;    // Convert hPa to in/Hg
  lcd.setCursor(12, 2);
  lcd.print("    ");
  lcd.setCursor(12, 2);
  String pressureString = String(pressure, 2);
  lcd.print(pressureString);
  lcd.print("mm");
}
//===============================================================================
//  getAltitude - Subroutine to get and print temperature
//===============================================================================
void getAltitude()
{
  altitude = bme.readAltitude(SEA_LEVEL_PRESSURE);
  // altitude = altitude * 3.28084;  // Convert meters to feet
  lcd.setCursor(12, 3);
  lcd.print("      ");
  lcd.setCursor(12, 3);
  String altitudeString = String(altitude, 0);
  lcd.print(altitudeString);
  lcd.print(" M");
}
